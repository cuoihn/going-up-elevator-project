export class FwtModel {
  id: number;
  name: string;
  description: string;
  phone: string;
  locationFwt: string;
}
