export class UserModel {
  id: number;
  firstName: string;
  lastName: string;
  username: string;
  email: string;
  age: number;
  roleId: number;
  isDelete: number;
  visible: number;
  createdAt: string;
  updatedAt: string;
}
