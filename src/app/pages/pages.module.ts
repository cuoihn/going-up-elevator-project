import { NgModule } from '@angular/core';
import { NbMenuModule } from '@nebular/theme';

import { ThemeModule } from '../@theme/theme.module';
import { PagesRoutingModule } from './pages-routing.module';
import { MiscellaneousModule } from './miscellaneous/miscellaneous.module';
import { PagesComponent } from './pages.component';

import { DashboardModule } from './dashboard/dashboard.module';
import { UsersModule } from './users/users.module';
import { RoleModule } from './role/role.module';
import { FwtModule } from './fwt/fwt.module';
import { EmailModule } from './email/email.module';
import { RequestModule } from './request/request.module';
import { ReportModule } from './report/report.module';
import { SmsModule } from './sms/sms.module';

@NgModule({
  imports: [
    PagesRoutingModule,
    ThemeModule,
    MiscellaneousModule,
    NbMenuModule,
    DashboardModule,
    UsersModule,
    RoleModule,
    FwtModule,
    EmailModule,
    RequestModule,
    ReportModule,
    SmsModule,
  ],
  declarations: [
    PagesComponent,
  ],
})
export class PagesModule {
}
