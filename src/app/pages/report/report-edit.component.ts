import {Component, OnInit, Inject} from '@angular/core';
import {Router, ActivatedRoute, ParamMap } from '@angular/router';
import {NbThemeService} from '@nebular/theme';
import {Location} from '@angular/common';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import { FwtModel } from '../../model/fwt.model';

@Component({
  selector: 'ngx-report-edit',
  templateUrl: './report-edit.component.html',
  styleUrls: ['./report-edit.component.scss'],
})
export class ReportEditComponent implements OnInit {
  fwt: FwtModel = new FwtModel();
  editForm: FormGroup;
  title: string = 'Create news';
  constructor(
    private router: Router,
    @Inject(ActivatedRoute) private routeA: ActivatedRoute,
    private _location: Location,
    private formBuilder: FormBuilder,
  ) {}

  ngOnInit(): void {
    this.routeA.params.subscribe(params => {
      this.fwt.id = params['id'];
      if (this.fwt.id > 0) {
        this.title = 'Update';
        this.fwt.id = 1;
        this.fwt.name = 'Lipsum'
        this.fwt.description = 'Lipsum description'
        this.fwt.phone = '0985611758';
        this.fwt.locationFwt = 'Australia';
      }
    });

    this.editForm = this.formBuilder.group({});
  }

  backClicked(): void {
    this._location.back();
  }

  onSubmit(): void {
    console.info(this.fwt);
  }
}
