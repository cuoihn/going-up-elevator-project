import {Component, OnInit} from '@angular/core';
import {NbWindowService} from '@nebular/theme';
import {LocalDataSource} from 'ng2-smart-table';
import {SmartTableData} from '../../@core/data/smart-table';

@Component({
  selector: 'ngx-role',
  templateUrl: './role.component.html',
  styleUrls: ['./role.component.scss'],
})
export class RoleComponent implements OnInit {
  settings = {
    actions: {position: 'right'},
    add: {
      addButtonContent: '<i class="nb-plus"></i>',
      createButtonContent: '<i class="nb-checkmark"></i>',
      cancelButtonContent: '<i class="nb-close"></i>',
    },
    edit: {
      editButtonContent: '<i class="nb-edit"></i>',
      saveButtonContent: '<i class="nb-checkmark"></i>',
      cancelButtonContent: '<i class="nb-close"></i>',
    },
    delete: {
      deleteButtonContent: '<i class="nb-trash"></i>',
      confirmDelete: true,
    },
    columns: {
      id: {
        title: 'ID',
        type: 'number',
      },
      name: {
        title: 'Name',
        type: 'string',
      },
      description: {
        title: 'Description',
        type: 'string',
      },
    },
  };

  source: LocalDataSource = new LocalDataSource();
  constructor(
    private service: SmartTableData,
  ) {
    const data = [
      {id: 1, name: 'Admin system', description: 'Admin system'},
      {id: 2, name: 'User permission', description: 'User permission'},
      {id: 3, name: 'Installation/FWT', description: 'Installation/FWT'},
      {id: 4, name: 'Email', description: 'Email'},
      {id: 5, name: 'SMS', description: 'SMS'},
      {id: 6, name: 'Requests', description: 'Requests'},
      {id: 6, n7me: 'Reporting', description: 'Reporting'},
    ];
    this.source.load(data);
  }

  ngOnInit(): void {}

  onDeleteConfirm(event): void {
    if (window.confirm('Are you sure you want to delete?')) {
      event.confirm.resolve();
    } else {
      event.confirm.reject();
    }
  }
}
