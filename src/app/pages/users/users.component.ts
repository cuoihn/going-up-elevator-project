import {Component, OnInit} from '@angular/core';
import {NbWindowService} from '@nebular/theme';
import {LocalDataSource} from 'ng2-smart-table';
import {SmartTableData} from '../../@core/data/smart-table';
import {WindowFormComponent} from './window-form/window-form.component';

@Component({
  selector: 'ngx-users',
  templateUrl: './users.component.html',
  styleUrls: ['./users.component.scss'],
})
export class UsersComponent implements OnInit {
  settings = {
    mode: 'external',
    actions: {add: true, edit: true, delete: true, position: 'right'},
    add: {
      addButtonContent: '<i class="nb-plus"></i>',
    },
    edit: {
      editButtonContent: '<i class="nb-edit"></i>',
    },
    delete: {
      deleteButtonContent: '<i class="nb-trash"></i>',
      confirmDelete: true,
    },
    columns: {
      id: {
        title: 'ID',
        type: 'number',
      },
      firstName: {
        title: 'First Name',
        type: 'string',
      },
      lastName: {
        title: 'Last Name',
        type: 'string',
      },
      username: {
        title: 'Username',
        type: 'string',
      },
      email: {
        title: 'E-mail',
        type: 'string',
      },
      age: {
        title: 'Age',
        type: 'number',
      },
    },
  };

  source: LocalDataSource = new LocalDataSource();
  constructor(
    private service: SmartTableData,
    private windowService: NbWindowService,
  ) {
    const data = this.service.getData();
    this.source.load(data);
  }

  ngOnInit(): void {}

  addUser(event): void {
    this.windowService.open(WindowFormComponent, {title: `Add new user`});
  }

  editUser(event): void {
    this.windowService.open(WindowFormComponent, {title: `Edit user info`, context: event.data});
  }

  onDeleteConfirm(event): void {
    if (window.confirm('Are you sure you want to delete?')) {
      event.confirm.resolve();
    } else {
      event.confirm.reject();
    }
  }
}
