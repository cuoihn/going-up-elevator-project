import {Component, OnInit} from '@angular/core';
import {NbWindowRef} from '@nebular/theme';
import {UserModel} from '../../../model/user.model';

@Component({
  selector: 'ngx-form-user',
  styleUrls: ['window-form.component.scss'],
  templateUrl: './window-form.component.html',
})
export class WindowFormComponent implements OnInit {
  userModel: UserModel = new UserModel();
  title: string = 'Create';
  constructor(public windowRef: NbWindowRef) {
    if (windowRef.config.context.hasOwnProperty('id')) {
      this.title = 'Update';
      this.userModel = windowRef.config.context as UserModel;
    }
  }

  ngOnInit(): void {
  }

  onSubmit() {}

  close() {
    this.windowRef.close();
  }
}
