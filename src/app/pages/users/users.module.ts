import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import {
  NbCardModule,
  NbIconModule,
  NbInputModule,
  NbWindowModule,
  NbButtonModule,
  NbCheckboxModule,
  NbRadioModule,
} from '@nebular/theme';

import { ThemeModule } from '../../@theme/theme.module';
import { UsersComponent } from './users.component';
import { Ng2SmartTableModule } from 'ng2-smart-table';
import { WindowFormComponent } from './window-form/window-form.component';

@NgModule({
  imports: [
    FormsModule,
    NbCardModule,
    NbIconModule,
    NbInputModule,
    NbButtonModule,
    NbCheckboxModule,
    NbRadioModule,
    ThemeModule,
    NbWindowModule.forChild(),
    Ng2SmartTableModule,
  ],
  declarations: [
    UsersComponent,
    WindowFormComponent,
  ],
  entryComponents: [
    WindowFormComponent,
  ],
})
export class UsersModule { }
