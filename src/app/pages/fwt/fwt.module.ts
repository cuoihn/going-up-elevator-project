import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import {
  NbCardModule,
  NbIconModule,
  NbInputModule,
  NbWindowModule,
  NbButtonModule,
  NbCheckboxModule,
  NbRadioModule,
} from '@nebular/theme';

import { ThemeModule } from '../../@theme/theme.module';
import { FwtComponent } from './fwt.component';
import { FwteditComponent } from './fwt-edit.component';
import { Ng2SmartTableModule } from 'ng2-smart-table';

@NgModule({
  imports: [
    FormsModule,
    NbCardModule,
    NbIconModule,
    NbInputModule,
    NbButtonModule,
    NbCheckboxModule,
    NbRadioModule,
    ThemeModule,
    NbWindowModule.forChild(),
    Ng2SmartTableModule,
  ],
  declarations: [
    FwtComponent,
    FwteditComponent,
  ],
  entryComponents: [
  ],
})
export class FwtModule { }
