import {Component, OnInit} from '@angular/core';
import {Router} from '@angular/router';

import {LocalDataSource} from 'ng2-smart-table';

@Component({
  selector: 'ngx-fwt',
  templateUrl: './fwt.component.html',
  styleUrls: ['./fwt.component.scss'],
})
export class FwtComponent implements OnInit {
  settings = {
    mode: 'external',
    actions: {add: true, edit: true, delete: true, position: 'right'},
    add: {
      addButtonContent: '<i class="nb-plus"></i>',
    },
    edit: {
      editButtonContent: '<i class="nb-edit"></i>',
    },
    delete: {
      deleteButtonContent: '<i class="nb-trash"></i>',
      confirmDelete: true,
    },
    columns: {
      id: {
        title: 'ID',
        type: 'number',
      },
      name: {
        title: 'Name',
        type: 'string',
      },
      description: {
        title: 'Description',
        type: 'string',
      },
      phone: {
        title: 'Phone',
        type: 'string',
      },
      locationFwt: {
        title: 'Location',
        type: 'string',
      },
    },
  };

  source: LocalDataSource = new LocalDataSource();
  constructor(
    private router: Router,
  ) {
    const data = [
      {id: 1, name: 'Lipsum', description: 'Lipsum description', phone: '0985611758', locationFwt: 'Australia'},
      {id: 2, name: 'Lipsum', description: 'Lipsum description', phone: '0985611758', locationFwt: 'Australia'},
      {id: 3, name: 'Lipsum', description: 'Lipsum description', phone: '0985611758', locationFwt: 'Australia'},
      {id: 4, name: 'Lipsum', description: 'Lipsum description', phone: '0985611758', locationFwt: 'Australia'},
      {id: 5, name: 'Lipsum', description: 'Lipsum description', phone: '0985611758', locationFwt: 'Australia'},
      {id: 6, name: 'Lipsum', description: 'Lipsum description', phone: '0985611758', locationFwt: 'Australia'},
      {id: 6, name: 'Lipsum', description: 'Lipsum description', phone: '0985611758', locationFwt: 'Australia'},
    ];
    this.source.load(data);
  }

  ngOnInit(): void {}

  addFwt(event): void {
    this.router.navigate(['/pages/fwt/news']);
  }

  editFwt(event): void {
    this.router.navigate(['/pages/fwt/edit/' + event.data.id]);
  }

  onDeleteConfirm(event): void {
    if (window.confirm('Are you sure you want to delete?')) {
      event.confirm.resolve();
    } else {
      event.confirm.reject();
    }
  }
}
