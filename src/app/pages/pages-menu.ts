import { NbMenuItem } from '@nebular/theme';

export const MENU_ITEMS: NbMenuItem[] = [
  {
    title: 'Dashboard',
    icon: 'home-outline',
    link: '/pages/dashboard',
    home: true,
  },
  {
    title: 'Account Management',
    icon: 'people-outline',
    children: [
      {
        title: 'User management',
        link: '/pages/users',
      },
      {
        title: 'Roles management',
        link: '/pages/roles',
      },
    ],
  },
  {
    title: 'Installation/FWT management',
    icon: 'map-outline',
    link: '/pages/fwt',
  },
  {
    title: 'Email management',
    icon: 'email-outline',
    link: '/pages/email',
  },
  {
    title: 'SMS management',
    icon: 'message-circle-outline',
    link: '/pages/sms',
  },
  {
    title: 'Request management',
    icon: 'question-mark-circle',
    link: '/pages/request',
  },
  {
    title: 'Reporting',
    icon: 'file-text-outline',
    children: [
      {
        title: 'Low signal strength',
        link: '/pages/report',
      },
      {
        title: 'Battery due for replacement',
        link: '/pages/report',
      },
      {
        title: 'Repair request management',
        link: '/pages/report',
      },
    ],
  },
  {
    title: 'Auth',
    icon: 'lock-outline',
    children: [
      {
        title: 'Login',
        link: '/auth/login',
      },
      {
        title: 'Request Password',
        link: '/auth/request-password',
      },
    ],
  },
];
