import { RouterModule, Routes } from '@angular/router';
import { NgModule } from '@angular/core';

import { NotFoundComponent } from './miscellaneous/not-found/not-found.component';
import { PagesComponent } from './pages.component';

import { DashboardComponent } from './dashboard/dashboard.component';
import { UsersComponent } from './users/users.component';
import { RoleComponent } from './role/role.component';
import { FwtComponent } from './fwt/fwt.component';
import { FwteditComponent } from './fwt/fwt-edit.component';
import { EmailComponent } from './email/email.component';
import { EmailEditComponent } from './email/email-edit.component';
import { ReportComponent } from './report/report.component';
import { ReportEditComponent } from './report/report-edit.component';
import { RequestComponent } from './request/request.component';
import { RequestEditComponent } from './request/request-edit.component';
import { SmsComponent } from './sms/sms.component';
import { SmsEditComponent } from './sms/sms-edit.component';


const routes: Routes = [{
  path: '',
  component: PagesComponent,
  children: [
    { path: 'dashboard', component: DashboardComponent },
    { path: 'users', component: UsersComponent },
    { path: 'roles', component: RoleComponent },
    { path: 'fwt', component: FwtComponent },
    { path: 'fwt/edit/:id', component: FwteditComponent },
    { path: 'fwt/news', component: FwteditComponent },
    { path: 'email', component: EmailComponent },
    { path: 'email/edit/:id', component: EmailEditComponent },
    { path: 'email/news', component: EmailEditComponent },
    { path: 'report', component: ReportComponent },
    { path: 'report/edit/:id', component: ReportEditComponent },
    { path: 'report/news', component: ReportEditComponent },
    { path: 'request', component: RequestComponent },
    { path: 'request/edit/:id', component: RequestEditComponent },
    { path: 'request/news', component: RequestEditComponent },
    { path: 'sms', component: SmsComponent },
    { path: 'sms/edit/:id', component: SmsEditComponent },
    { path: 'sms/news', component: SmsEditComponent },
    { path: '', redirectTo: 'dashboard', pathMatch: 'full' },
    { path: '**', component: NotFoundComponent },
  ],
}];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class PagesRoutingModule {
}
